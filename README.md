# LESAUTRES WEBSITE

test

This is the web UI for https://lesautres.com. It has been created with create-react-app.

You can find more about create-react-app in the CREATE_REACT_APP_README.md.

## Stuff to know

- It's using Prettier to automatically format your code.
- It's using Flow for static type checking. It'll be super handy for refactoring.