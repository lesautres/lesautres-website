// @flow

import React, { Component } from 'react';
import './Dashboard.css';
import 'mdi/css/materialdesignicons.min.css';
import $ from 'jquery';
import MapSvg from './_assets/map.svg.js';
import Chart from 'chart.js/dist/Chart';

import playlists from './_data/playlists';

import Timeline from './_Timeline';
import TrackList from './_TrackList';

console.log(playlists[0].name);

type Props = {};

class Dashboard extends Component<Props> {
  _highlightCountries = () => {
    for (let track of playlists[0].tracks) {
      console.log(track);
      for (let country of track.countries_iso) {
        console.log(country);
        $(`.${country}`).css('fill', 'orange');
      }
    }
  };

  componentDidMount() {
    this._highlightCountries();
  }

  render() {
    return (
      <main className="col-md-12 dashboard">
        <div className="row">
          <section className="col-md-4">
            <header>
              <h2>Blend #7 • Jazzy Java</h2>
              <p>Lorem ipsum dolor sit amer.</p>
            </header>
            <iframe
              src="https://open.spotify.com/embed?uri=spotify:user:lesautres_co:playlist:6PzQns1813zFQysuahcnev"
              width="300"
              height="400"
              frameborder="0"
              allowtransparency="true"
            />
          </section>
          <section className="col-md-8">
            <TrackList tracks={playlists[0].tracks} />
            <div className="map">
              <MapSvg id="world" />
            </div>
            <div className="timeline">
              <Timeline />
            </div>
          </section>
        </div>
      </main>
    );
  }
}

export default Dashboard;
