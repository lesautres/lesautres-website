// @flow

import React, { Component } from 'react';

import Chart from 'chart.js/dist/Chart';
import $ from 'jquery';
import './Dashboard.css';

// The data is currently static. It needs to be updated manually everyday.
//import { commitsData } from './_data/commitsData';

type Props = {};

class Timeline extends Component<Props> {
  componentDidMount() {
    const ctx = $('#timelineChart');
    const today = Date.now();
    new Chart(ctx, {
      type: 'line',
      data: {
        labels: ['#1', '#2', '#3', '#4', '#5', '#6', '#7'],
        datasets: [
          {
            backgroundColor: '#FFAB00',
            borderWidth: 0,
            showLine: false,
            pointRadius: 10,
            pointHoverRadius: 10,
            pointHoverBackgroundColor: '#FF991F',
            fill: false,
            data: [1968, 2017, 1988, 2012]
          }
        ]
      },
      options: {
        legend: {
          display: false
        },
        scales: {
          yAxes: [
            {
              display: true,
              ticks: {
                suggestedMin: 1960
              }
            }
          ]
        }
      }
    });
  }

  render() {
    return (
      <div className="timeline">
        <canvas id="timelineChart" className="" width="200" />
      </div>
    );
  }
}

export default Timeline;
