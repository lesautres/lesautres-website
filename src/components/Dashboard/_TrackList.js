// @flow

import React, { Component } from 'react';
import Countries from 'i18n-iso-countries';

// The data is currently static. It needs to be updated manually everyday.
//import { commitsData } from './_data/commitsData';

type Props = {
  tracks: any
};

class TrackList extends Component<Props> {
  _renderCountries(iso_codes: any) {
    const countries = iso_codes.map(iso => Countries.getName(iso, 'en'));

    return countries.join(' & ');
  }

  render() {
    const tracksList = this.props.tracks.map(track => (
      <li>
        {track.title} by {track.author} -
        {this._renderCountries(track.countries_iso)},
        {track.year}
      </li>
    ));
    return <ol>{tracksList}</ol>;
  }
}

export default TrackList;
