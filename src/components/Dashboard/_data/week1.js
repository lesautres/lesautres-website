const playlist = {
  name: 'Mr. Strong',
  spotify_id: '2yqzu2rzsrs7BubHhNuErA',
  tracks: [
    {
      title: 'Dasi Ko',
      author: 'Bella Bello',
      countries_iso: ['tg'],
      year: 1973
    },
    {
      title: 'Jaiyede Afro',
      author: 'Orlando Julius & the Heliocentrics',
      countries_iso: ['ng', 'gb'],
      year: 2014
    },
    {
      title: 'Equalisation of Trouser & Pant',
      author: 'Fela Kuti',
      countries_iso: ['ng'],
      year: 1973
    },
    {
      title: 'Road 2 Accra',
      author: 'Nomad',
      countries_iso: ['at', 'gh'],
      year: 2014
    },
    {
      title: 'Tropical Heat',
      author: 'Bacao Rhythm & Steel Band',
      countries_iso: ['de'],
      year: 2016
    },
    {
      title: 'Heaven & Hell',
      author: 'William Onyeabor',
      countries_iso: ['ng'],
      year: 1982
    },
    {
      title: 'Sanza Tristesse',
      author: 'Francis Bebey',
      countries_iso: ['cm'],
      year: 1982
    }
  ]
};

export default playlist;
