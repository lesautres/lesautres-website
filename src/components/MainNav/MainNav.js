import React, { Component } from 'react';
import './MainNav.css';
import logo from './_img/logo.svg';

type Props = {};

class MainNav extends Component<Props> {
  render() {
    return (
      <div className="row main-nav">
        <div className="col-md-12">
          <a href="/"><img src={logo} alt="Les Autres" /></a>
          <p className="baseline">Bridging cultures through creativity</p>
        </div>
      </div>
    );
  }
}

export default MainNav;
