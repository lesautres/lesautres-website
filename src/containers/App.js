// @flow
import React, { Component } from 'react';
import MainNav from '../components/MainNav';
import Dashboard from '../components/Dashboard';
import './App.css';

type Props = {};

class App extends Component<Props> {
  render() {
    return (
      <div className="container-fluid App">
        <MainNav />
        <div className="row">
          <Dashboard />
        </div>
      </div>
    );
  }
}

export default App;
