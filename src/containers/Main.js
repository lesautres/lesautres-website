import React, { Component } from 'react';
import './Main.css';
import logo from '../img/logo.svg';

type Props = {};

class App extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      playlists: [
        {
          id: '2yqzu2rzsrs7BubHhNuErA',
          title: 'Blend #1 • Mr. Strong',
          description: 'A perfect introduction to les autres.'
        },
        {
          id: '6TdHwuqD3bHoNpRCrH3VAd',
          title: 'Blend #2 • Café Bica',
          description: 'Slow down in synth major.'
        },
        {
          id: '2n0IzEl1MBbLrNlEe4QeJa',
          title: 'Blend #3 • Petit Noir',
          description: 'Wake up with to some afrolectronica.'
        },
        {
          id: '0VabELHyL6qOQrGSgFGtSL',
          title: 'Blend #4 • Grains Electriques',
          description: "Rock & Afric'all - Afroblues."
        },
        {
          id: '1nxINIwWrIgwvHtELSjJVS',
          title: 'Blend #5 • Bica Nocturne',
          description: 'F***ing beats. Listen at your own risk.'
        },
        {
          id: '6vZJfr9plxOBlyomKG7PoU',
          title: 'Blend #6 • Allongé Serré',
          description: "You'll travel safe down memory lane."
        },
        {
          id: '6PzQns1813zFQysuahcnev',
          title: 'Blend #7 • Jazzy Java',
          description: 'Not for everyone, but sooo goood.'
        },
        {
          id: '4jpetr5R59H4L1gCHpMYXO',
          title: 'Blend #8 • Macchiavelto',
          description: 'For your morning walk. Be human.'
        },
        {
          id: '7EJpLlh9mRzoqvLKZdwMD0',
          title: 'Blend #9 • Filtré Vibré',
          description: 'AfroDisco afternoons welcome.'
        },
        {
          id: '4LwGFEr1w0e906JalzbjzN',
          title: 'Blend #10 • Coltrafé',
          description: 'Long is the life. Slow is the day.'
        },
        {
          id: '0fWvKfQafI4aabdIuZnJ1c',
          title: 'Blend #11 • Maison Café',
          description: 'House. Mu. Sic. !.'
        },
        {
          id: '2uwuvPUlKuf9WAJ7YGjvxE',
          title: 'Blend #12 • Jackalakafé',
          description: 'Sisi, nononon, wééééé'
        },
        {
          id: '6WyzHArrckQXMjMjO2a2Ls',
          title: 'Blend #13 • Robusta Salsa',
          description: 'Pour up (Drank), Africa (Drank)'
        },
        {
          id: '22ArZOd2h5kfLhhP0eIjiA',
          title: 'Blend #14 • Dark is the knight',
          description: 'Not an espresso, sip it slow-ly.'
        }
      ]
    };
  }

  render() {
    const { playlists } = this.state;
    console.log(playlists);
    const playlistItems = playlists.map(p => (
      <div key={p.id} className="playlist">
        <h4>
          <a
            href={`https://open.spotify.com/user/lesautres_co/playlist/${p.id}`}
          >
            {p.title}
          </a>
        </h4>
        <p>{p.description}</p>
        <iframe
          title={p.title}
          src={`https://open.spotify.com/embed?uri=spotify:user:lesautres_co:playlist:${p.id}`}
          width="300"
          height="400"
          frameBorder="0"
          allowTransparency="true"
        />
      </div>
    ));

    return (
      <div className="mainContainer">
        <div className="main">
          <div className="logo">
            <a href="/">
              <img src={logo} alt="Les Autres" />
            </a>
            <p className="baseline">Bridging cultures through creativity</p>
          </div>
          <div className="playlists-container">{playlistItems}</div>
        </div>
      </div>
    );
  }
}

export default App;
